# AdboxGoogleAnalytics

[![CI Status](https://img.shields.io/travis/Aleks C. Barragan/AdboxGoogleAnalytics.svg?style=flat)](https://travis-ci.org/Aleks C. Barragan/AdboxGoogleAnalytics)
[![Version](https://img.shields.io/cocoapods/v/AdboxGoogleAnalytics.svg?style=flat)](https://cocoapods.org/pods/AdboxGoogleAnalytics)
[![License](https://img.shields.io/cocoapods/l/AdboxGoogleAnalytics.svg?style=flat)](https://cocoapods.org/pods/AdboxGoogleAnalytics)
[![Platform](https://img.shields.io/cocoapods/p/AdboxGoogleAnalytics.svg?style=flat)](https://cocoapods.org/pods/AdboxGoogleAnalytics)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AdboxGoogleAnalytics is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AdboxGoogleAnalytics'
```

## Author

Aleks C. Barragan, leks.bar@icloud.com

## License

AdboxGoogleAnalytics is available under the MIT license. See the LICENSE file for more info.
